__author__ = 'abeedsarker'


import os
import nltk,string
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
from sklearn.feature_extraction import DictVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
#from KaggleWord2VecUtility import KaggleWord2VecUtility
import pandas as pd
from sklearn import svm
import numpy as np
import glob
import json
import re
from bs4 import BeautifulSoup
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from nltk.stem.porter import *
import aspell
s = aspell.Speller('lang', 'en')

class FeatureExtractionUtilities:
    @staticmethod
    def getstructuralfeatures(processed_data):
        lens = FeatureExtractionUtilities.getreviewlengths(processed_data)
        numsents = FeatureExtractionUtilities.getnumsentences(processed_data)
        avelengths = FeatureExtractionUtilities.getaveragesentlengths(processed_data)
    
        features = map(list.__add__,lens,avelengths)
        features = map(list.__add__,features,numsents)
        return features
    
    @staticmethod
    def getreviewlengths(processed_data):
        lengths = []
        for d in processed_data:
            items = d.split()
            lengths.append([len(items)])
    
        return lengths
    @staticmethod
    def getnumsentences(processed_data):
        numsents = []
        for d in processed_data:
            items = nltk.sent_tokenize(d)
            numsents.append([len(items)])
        return numsents
    @staticmethod
    def getaveragesentlengths(processed_data):
        avelengths = []
        for d in processed_data:
            items = nltk.sent_tokenize(d)
            words = d.split()
            numsents = len(items)
            numwords = len(words)
            avelengths.append([numwords/(numsents+0.0)])
        return avelengths
