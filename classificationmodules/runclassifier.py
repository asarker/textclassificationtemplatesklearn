'''
Created on Nov 23, 2015

@author: asarker
'''

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
import string,nltk,os,re
from nltk.stem.porter import *
from sklearn import preprocessing
from sklearn.metrics import accuracy_score
import numpy as np
from nltk.corpus import stopwords
from sklearn import svm
from featureextractionmodules.FeatureExtractionUtilities import FeatureExtractionUtilities
st =  stopwords.words('english')


stemmer = PorterStemmer()
import codecs
def loadDataAsDataFrame(f_path):
    '''
        Given a path, loads a data set and puts it into a dataframe
    '''
    datalist = []
    count = 0
    infile = codecs.open(f_path,'r3',encoding='utf-8')
    for line in infile:
        instance_dict = {}
        items = line.split('\t')
        instance_dict['id1'] = items[0]
        instance_dict['id2'] = items[1]
        instance_dict['class'] = items[2]
        try:
            instance_dict['text'] = items[3].decode('iso-8859-1').encode('utf8')
        except UnicodeEncodeError:
            ptext = items[3]
            instance_dict['text'] = ptext
        datalist.append(instance_dict)
        print instance_dict
        count+=1
    return pd.DataFrame(datalist)

def text_to_words(raw_text):
    '''
        * Function to convert a raw text into  list of tokens.
        * Pre-processing steps go here.
    '''
    # 1. Remove HTML
    #review_text = BeautifulSoup(raw_review).get_text()
    #
    # 2. Remove non-letters        
    #letters_only = re.sub("[^a-zA-Z]", " ", review_text)
    #
    # 3. Convert to lower case, stem, and split into individual words
    words = [stemmer.stem(w) for w in raw_text.lower().split()]
    #
    # 4. In Python, searching a set is much faster than searching
    #   a list, so convert the stop words to a set
    #stops = set(stopwords.words("english"))
    # 
    # 5. Remove stop words
    #meaningful_words = [w for w in words if not w in stops]
    #
    # 6. Join the words back into one string separated by space, 
    # and return the result.
    #print ( " ".join( words ))
    return( " ".join( words ))
    
if __name__ == '__main__':
    ''' load the training and test sets.. often it might be the same file and needs to
        be separated later on
    '''
    f_path = '/home/asarker/workspace/TextClassificationTemplateSKLearn/Data/semeval_dev_train.txt'
    
    all_data = loadDataAsDataFrame(f_path)
    
    test_data = all_data[int(len(all_data)*0.8):]
    training_data = all_data[:int(len(all_data)*0.8)]
    print len(test_data)
    print len(training_data)
    
    
    #test_data = loadDataAsDataFrame(f_path)
    training_data_texts = training_data['text']
 

    vectorizer = CountVectorizer(ngram_range=(1,3), analyzer = "word", tokenizer = None, preprocessor = None, max_features = 5000)

    tokens = []
    processed_training_data = []
    for t in training_data_texts:
        toks = text_to_words(t)
          
        processed_training_data.append(toks)                              
    trained_data = vectorizer.fit_transform(processed_training_data).toarray()
    train_structural_features = FeatureExtractionUtilities.getstructuralfeatures(processed_training_data)
    scaler1 = preprocessing.StandardScaler().fit(train_structural_features)
    train_structural_features = scaler1.transform(train_structural_features)

    #train_lexical_features,clusters = FeatureExtractionUtilities.getlexicalfeatures(processed_training_data,processed_data_df1)
    #scaler2 = preprocessing.StandardScaler().fit(train_lexical_features)
    #train_lexical_features = scaler2.transform(train_lexical_features)
    
    #trained_clusters = classvectorizer.fit_transform(clusters).toarray()

    trained_data = np.concatenate((trained_data,train_structural_features),axis=1)
    #trained_data = np.concatenate((trained_data,train_lexical_features),axis=1)
    #trained_data = np.concatenate((trained_data,trained_clusters),axis=1)
    neg_weight = 2.1
    pos_weight =0.9
    neut_weight = 0.7
    svm_classifier = svm.SVC(C=32, cache_size=200, class_weight={'positive':pos_weight,'negative':neg_weight,'neutral':neut_weight}, coef0=0.0, degree=3,
              gamma=0.0, kernel='rbf', max_iter=-1, probability=True, random_state=None,
              shrinking=True, tol=0.001, verbose=False)
    svm_classifier = svm_classifier.fit( trained_data, training_data["class"] )

    #----------- ----------------

    test_data_texts = test_data["text"]
    processed_testing_data = []
    for t in test_data_texts:
        toks = text_to_words(t)  
        processed_testing_data.append(toks)                           
    testing_data = vectorizer.transform(processed_testing_data)
    testing_data = testing_data.toarray()
    test_structural_features = FeatureExtractionUtilities.getstructuralfeatures(processed_testing_data)
    test_structural_features = scaler1.transform(test_structural_features)
    
    #test_lexical_features,tclusters = FeatureExtractionUtilities.getlexicalfeatures(processed_testing_data,processed_data_df2)
    #test_lexical_features = scaler2.transform(test_lexical_features)
    
    #test_clusters = classvectorizer.transform(tclusters).toarray()
    
    testing_data = np.concatenate((testing_data,test_structural_features),axis=1)
    #testing_data = np.concatenate((testing_data,test_lexical_features),axis=1)
    #testing_data = np.concatenate((testing_data,test_clusters),axis=1)
    #test = test.toarray()
    
    fd = nltk.FreqDist(all_data["class"])
    for k in fd.keys():
        print k,'\t',fd[k]
    # Use the random svm_classifier to make sentiment label predictions
    result = svm_classifier.predict(testing_data)
    
    import evaluation
    print accuracy_score(test_data["class"],result)   
    evaluation.evaluateSemEval(test_data["class"], result)
    #print sum(data_df1["sentiment"])     
    #print len(data_df1["sentiment"])
          