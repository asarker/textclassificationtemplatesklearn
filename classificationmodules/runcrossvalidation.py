'''
Created on Nov 23, 2015

@author: asarker
'''

'''
Created on Nov 23, 2015

@author: asarker
'''
import evaluation
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
import string,nltk,os,re
from nltk.stem.porter import *
from sklearn import preprocessing
from sklearn.metrics import accuracy_score
import numpy as np
from nltk.corpus import stopwords
from sklearn import svm,cross_validation

from featureextractionmodules.FeatureExtractionUtilities import FeatureExtractionUtilities
st =  stopwords.words('english')

stemmer = PorterStemmer()
import codecs
def loadDataAsDataFrame(f_path):
    '''
        Given a path, loads a data set and puts it into a dataframe
    '''
    datalist = []
    count = 0
    infile = codecs.open(f_path,'r3',encoding='utf-8')
    for line in infile:
        instance_dict = {}
        items = line.split('\t')
        instance_dict['id1'] = items[0]
        instance_dict['id2'] = items[1]
        instance_dict['class'] = items[2]
        try:
            instance_dict['text'] = items[3].decode('iso-8859-1').encode('utf8')
        except UnicodeEncodeError:
            ptext = items[3]
            instance_dict['text'] = ptext
        datalist.append(instance_dict)
        print instance_dict
        count+=1
    return pd.DataFrame(datalist)

def text_to_words(raw_text):
    '''
        * Function to convert a raw text into  list of tokens.
        * Pre-processing steps go here.
    '''
    # 1. Remove HTML
    #review_text = BeautifulSoup(raw_review).get_text()
    #
    # 2. Remove non-letters        
    #letters_only = re.sub("[^a-zA-Z]", " ", review_text)
    #
    # 3. Convert to lower case, stem, and split into individual words
    words = [stemmer.stem(w) for w in raw_text.lower().split()]
    #
    # 4. In Python, searching a set is much faster than searching
    #   a list, so convert the stop words to a set
    #stops = set(stopwords.words("english"))
    # 
    # 5. Remove stop words
    #meaningful_words = [w for w in words if not w in stops]
    #
    # 6. Join the words back into one string separated by space, 
    # and return the result.
    #print ( " ".join( words ))
    return( " ".join( words ))
    
if __name__ == '__main__':
    ''' load the training and test sets.. often it might be the same file and needs to
        be separated later on
    '''
    f_path = '/home/asarker/workspace/TextClassificationTemplateSKLearn/Data/semeval_dev_train.txt'
    training_data = loadDataAsDataFrame(f_path)
    #test_data = loadDataAsDataFrame(f_path)
    training_data_texts = training_data['text']
 

    vectorizer = CountVectorizer(ngram_range=(1,3), analyzer = "word", tokenizer = None, preprocessor = None, max_features = 5000)

    tokens = []
    processed_training_data = []
    for t in training_data_texts:
        toks = text_to_words(t)
          
        processed_training_data.append(toks)                              
    trained_data = vectorizer.fit_transform(processed_training_data).toarray()
    train_structural_features = FeatureExtractionUtilities.getstructuralfeatures(processed_training_data)
    scaler1 = preprocessing.StandardScaler().fit(train_structural_features)
    train_structural_features = scaler1.transform(train_structural_features)

    #train_lexical_features,clusters = FeatureExtractionUtilities.getlexicalfeatures(processed_training_data,processed_data_df1)
    #scaler2 = preprocessing.StandardScaler().fit(train_lexical_features)
    #train_lexical_features = scaler2.transform(train_lexical_features)
    
    #trained_clusters = classvectorizer.fit_transform(clusters).toarray()

    trained_data = np.concatenate((trained_data,train_structural_features),axis=1)
    #trained_data = np.concatenate((trained_data,train_lexical_features),axis=1)
    #trained_data = np.concatenate((trained_data,trained_clusters),axis=1)
    score_dict = {}
    
    cost_vec = [0.125,0.25,0.5,1,2,4,8,16,32,64,128,256,512,1024]
    gamma_vec = [0.0,0.0001,0.001,0.01,0.1,0.25,0.5,1,2,4,8]
    neg_weight = 2.1
    pos_weight =0.9
    neut_weight = 0.7
    folds = 2
    for c in cost_vec:
        for g in gamma_vec:
            print 'Running config for.. cost = ' + str(c)+ ' gamma = ' + str(g) + '...'
            svm_classifier = svm.SVC(C=c, cache_size=200, class_weight={'positive':pos_weight,'negative':neg_weight,'neutral':neut_weight}, coef0=0.0, degree=3,
                      gamma=g, kernel='rbf', max_iter=-1, probability=True, random_state=None,
                      shrinking=True, tol=0.001, verbose=False)
            #svm_classifier = svm_classifier.fit( trained_data, training_data["class"] )
            
            scores = cross_validation.cross_val_score(svm_classifier, trained_data, training_data['class'], n_jobs=3, cv=folds,scoring='f1_macro')
            id_ = 'cost-'+str(c)+'-gamma-'+str(g)
            print id_ + '\t' + str(scores.mean())
            #score_dict[id_] = evaluation.
    import operator
    
    best = max(score_dict.iteritems(), key=operator.itemgetter(1))[0]
    print 'Best configuration via ' + str(folds) + ' cross-validation is: ' + str(best)
    print 'Best micro-averaged f-score: ' + str(score_dict[best]) 